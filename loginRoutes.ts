import express from 'express';
import { loginController } from './main';


export const loginRoutes = express.Router();
// loginRoutes.post('/memberLogin', loginController.memberLogin.bind(loginController));
loginRoutes.post('/memberLogin', loginController.memberLogin);
loginRoutes.post('/contributorLogin', loginController.contributorLogin);
loginRoutes.get('/loginStatus', loginController.getLoginStatus);
loginRoutes.get('/logOut', loginController.logout);