//webcam
let webcam_output, poseNet;
let poses = [];
//recording
let recording = false;
let recorder, blob;
let chunks = [];
//body part selected
let position;
//for setTimeout
let timeout;

function setup() {
    const canvas = createCanvas(640, 480);
    canvas.parent(document.querySelector('#canvas'));
    webcam_output = createCapture(VIDEO);
    webcam_output.size(width, height);

    poseNet = ml5.poseNet(webcam_output, 'single', modelReady);  //'single' or 'multiple'
    poseNet.on('pose', function (results) {
        poses = results;
    });

    webcam_output.hide();
    record();
}

function modelReady() {
    document.querySelector('#status').style.display = 'none';
    recordButton.disabled = false;
}

function draw() {
    image(webcam_output, 0, 0, width, height);
    drawKeypoints();
    drawSkeleton();
    selectPosition();
}

function drawKeypoints() {
    if (poses.length){
    // for (let i = 0; i < poses.length; i++) {
        let pose = poses[0].pose;
        for (let j = 0; j < pose.keypoints.length; j++) {
            let keypoint = pose.keypoints[j];
            if (keypoint.score > 0.5) {
                fill(0, 0, 255);
                noStroke();
                ellipse(keypoint.position.x, keypoint.position.y, 10, 10);
            }
        }
    }
}

function drawSkeleton() {
    if (poses.length){
    // for (let i = 0; i < poses.length; i++) {
        let skeleton = poses[0].skeleton;
        for (let j = 0; j < skeleton.length; j++) {
            let startPoint = skeleton[j][0];
            let endPoint = skeleton[j][1];
            stroke(0, 255, 0);
            line(startPoint.position.x, startPoint.position.y, endPoint.position.x, endPoint.position.y);
        }
    }
}

// https://github.com/tensorflow/tfjs-models/tree/master/posenet for the keypoints
function selectPosition(){
    if (position === "leftShoulder") {
        const angle = calculateAngle(11,7,5)
        angleContainer.innerText = "Left Shoulder Angle: " + angle
    }else if (position === "rightShoulder"){
        const angle = calculateAngle(12,8,6)
        angleContainer.innerText = "Right Shoulder Angle: " + angle
    }else if (position === "leftElbow"){
        const angle = calculateAngle(5,9,7)
        angleContainer.innerText = "Left Elbow Angle: " + angle
    }else if (position === "rightElbow"){
        const angle = calculateAngle(6,10,8)
        angleContainer.innerText = "Right Elbow Angle: " + angle
    }else if (position === "leftHip"){
        const angle = calculateAngle(5,13,11)
        angleContainer.innerText = "Left Hip Angle: " + angle
    }else if (position === "rightHip"){
        const angle = calculateAngle(6,14,12)
        angleContainer.innerText = "Right Hip Angle: " + angle
    }else if (position === "leftKnee"){
        const angle = calculateAngle(11,15,13)
        angleContainer.innerText = "Left Knee Angle: " + angle
    }else if (position === "rightKnee"){
        const angle = calculateAngle(12,16,14)
        angleContainer.innerText = "Right Knee Angle: " + angle
    }else{
        angleContainer.innerText = "Please Select a Body Part for the Angle"
    }
}

function calculateAngle(a,b,keypoint){
    let pose = poses[0].pose;
    const dist1 = calculateDistance(pose.keypoints[keypoint].position,pose.keypoints[b].position)
    const dist2 = calculateDistance(pose.keypoints[keypoint].position,pose.keypoints[a].position)
    const oppositeSide = calculateDistance(pose.keypoints[b].position,pose.keypoints[a].position)    
    return cosineLaw(dist1,dist2,oppositeSide) + '°'
}

function calculateDistance(a,b){
    return ((a.x-b.x)**2 + (a.y-b.y)**2)**0.5
}

function cosineLaw(a,b,c){
    return Math.round((Math.acos((a**2+b**2-c**2)/(2*a*b))/Math.PI)*180)
}

function record() {
    chunks.length = 0;
    let stream = document.querySelector('canvas').captureStream();
    recorder = new MediaRecorder(stream);
    recorder.ondataavailable = (e) => {
        if (e.data.size) {
            chunks.push(e.data);
        }
    };
    // recorder.onstart = record;
    recorder.onstop = exportVideo;
}

function exportVideo(e) {
    blob = new Blob(chunks, { 'type' : 'video/webm' });

    // Draw video to screen
    videoElement.style.display = "block"
    videoElement.controls = true;
    videoElement.src = window.URL.createObjectURL(blob);
    
    // Download the video 
    // let url = URL.createObjectURL(blob);
    // let a = document.createElement('a');
    // document.body.appendChild(a);
    // a.style = 'display: none';
    // a.href = url;
    // a.download = `${Date.now()}.webm`;
    // a.click();
    // window.URL.revokeObjectURL(url);
}

function recordingButton() {
    recording = !recording

    if (recording) {
        recorder.start();
        recordButton.innerText = 'Stop Recording'
        timeout = setTimeout(()=>{recordButton.click()}, 30000)
        uploadButton.disabled = true
    } 
    
    if (!recording) {
        clearTimeout(timeout)
        recorder.stop();
        record();
        recordButton.innerText = 'Start Recording'
        previewText.style.display = 'none'
        uploadButton.disabled = false
    }
}

async function uploadVideo(){
    uploadButton.innerText = 'Uploading...'
    const formData = new FormData();
    formData.append('video', blob);
    const res = await fetch('/upload', {
        method:"POST",
        body: formData
    })
    const result = await res.json();
    if (result.success){
        uploadButton.disabled = true
        uploadButton.innerText = 'Upload'
        alert('Uploaded!')
        previewText.style.display = 'block'
        videoElement.style.display = "none"
    }else{
        uploadButton.innerText = 'Upload'
        alert('Upload Failed')
    }
}

const angleContainer = document.querySelector('.angle-container')
const previewText = document.querySelector('.preview-text')
const recordButton = document.querySelector('.record-button')
recordButton.onclick = recordingButton;
recordButton.disabled = true;
const uploadButton = document.querySelector('.upload-button')
uploadButton.onclick = uploadVideo;
uploadButton.disabled = true;
const dropDownMenu = document.querySelector('.drop-down-menu')
dropDownMenu.addEventListener("change",()=>{position = dropDownMenu.value})
const videoElement = document.querySelector('#video-preview');

