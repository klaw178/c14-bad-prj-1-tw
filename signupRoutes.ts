import express from 'express';
import { signupController, upload } from './main';


export const signupRoutes = express.Router();
signupRoutes.post('/memberRegistration', upload.single('image'), signupController.memberRegistration);
signupRoutes.post('/contributorRegistration', upload.single('image'), signupController.contributorRegistration);