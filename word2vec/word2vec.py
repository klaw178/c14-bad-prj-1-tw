# %%
# Importing modules

import json
import nltk
import os
import pickle
import re
import sys
import gensim
import matplotlib.pyplot as plt
import numpy as py
import pandas as pd
# nltk.download('punkt') (run once only!)
# nltk.download('stopwords') (run once only!)
# nltk.download('wordnet') (run once only!)
from nltk.stem import WordNetLemmatizer, PorterStemmer
from nltk.corpus import stopwords
from gensim.models import Word2Vec
from sklearn.cluster import KMeans
from sklearn.manifold import TSNE
ps = PorterStemmer()
lemmatizer = WordNetLemmatizer()
stop_words = set(stopwords.words('English'))

# %%
# Create a model with scrapped content (deprecated!)

# with open('../scrapping/filteredContent.json', encoding="utf-8") as json_file:
#     articles = json.load(json_file)

#     article_text = ''
#     for article in articles:
#         article['title'] = ''
#         article = article.values()
#         for paragraph in article:
#             paragraph = paragraph + ' '
#             article_text += paragraph
#             pass
#         pass

#     sentences = nltk.sent_tokenize(article_text)
#     sentences_array = []

#     for sentence in sentences:
#         sentence = sentence.lower()
#         sentence = re.sub('[^a-zA-Z]', ' ', sentence)
#         sentence = re.sub(r'\s+', ' ', sentence)
#         sentences_array.append(nltk.word_tokenize(sentence))
#         pass

#     for i in range(len(sentences_array)):
#         sentences_array[i] = [lemmatizer.lemmatize(word) for word in sentences_array[i] if word not in stop_words]
#         pass

#     print(sentences_array)

#     model = Word2Vec(sentences=sentences_array, size=100, window=5, min_count=1, workers=4)
#     model.save(os.path.join(os.getcwd(), 'word2vec.model'))
#     pass

# %%
# Create model with Google trained vectors (run once only!)

# model = gensim.models.KeyedVectors.load_word2vec_format(os.path.join(os.getcwd(), 'GoogleNews-vectors-negative300.bin'), binary=True)

# %%
# Saving the model (run once only!)

model.save(os.path.join(os.getcwd(), 'word2vec_Google.model'))

# %%
# Load the pre-trained model

model = gensim.models.KeyedVectors.load(
    os.path.join(os.getcwd(), 'word2vec_Google.model'))

# %%
# Test the model

vector = model.wv['shoulder']
sims = model.wv.most_similar('shoulder', topn=100)
print(sims)

# %%
# Creating vector matrix and related lists with parts of scrapped content (run depends on model and needs!)


def gen_list():
    with open('../ngram/unigram_keywords.json', encoding="utf-8") as json_file:
        keyword_dict = json.load(json_file)
    sim_vector_matrix = []
    sim_list = []
    correct_keyword_list = []
    wrong_keyword_list = []
    keyword_list = list(keyword_dict.keys())
    for key in keyword_list:
        try:
            keyword_vector = model.wv[key]
            correct_keyword_list.append(key)
            sims = model.wv.most_similar(key, topn=100)
            for sim in sims:
                vector = model.wv[sim[0]]
                sim_vector_matrix.append(vector)
                sim_list.append(sim[0])
        except KeyError as e:
            wrong_keyword_list.append(key)
    sim_vector_matrix = py.array(sim_vector_matrix)
    return [sim_vector_matrix, sim_list, correct_keyword_list, wrong_keyword_list]


def write_list():
    X = gen_list()
    py.save('../word2vec/word2vec_sim_vector_matrix', X[0])
    with open('../word2vec/word2vec_set.json', 'w') as json_file:
        json.dump([X[1], X[2], X[3]], json_file, indent=4)


write_list()

# %%
# Saving K Means Clustering models with n clusters


def km(n):
    with open('../word2vec/word2vec_sim_vector_matrix.npy', 'rb') as sim_vector_matrix:
        X = py.load(sim_vector_matrix)
        print(len(X))
        NUM_CLUSTERS = n
        km = KMeans(
            n_clusters=NUM_CLUSTERS, init='random',
            n_init=10, max_iter=2000,
            tol=1e-04, random_state=2
        )
        return km.fit_predict(X)


def dump_km(n):
    model_name = f"y_km_{n}.pkl"
    model = km(n)
    pickle.dump(model, open(model_name, "wb"))

# dump_km(200)


# %%
# Visualize the result

def visualize(n, src, dest):
    with open('../word2vec/word2vec_set.json', encoding="utf-8") as json_file:
        word_list = json.load(json_file)
        vocabs = word_list[0]
        model = pickle.load(open(src, 'rb'))
        groups_vocabs = {}
        for i in range(n):
            vocab_list = []
            for j in range(len(vocabs)):
                if model[j] == i:
                    vocab_list.append(vocabs[j])
            groups_vocabs[i] = vocab_list
        with open(dest, 'w') as json_file:
            json.dump(groups_vocabs, json_file, indent=4)
        print(len(vocabs))


visualize(200, "y_km_200.pkl", '../word2vec/y_km_200.json')

# %%
# Plot the data (run on Colab!)


def plot():
    vocabs = []
    with open('../word2vec/word2vec_sim_vector_matrix.npy', 'rb') as sim_vector_matrix:
        X = py.load(sim_vector_matrix)
    with open('../word2vec/word2vec_set.json', 'rb') as json_file:
        word_list = json.load(json_file)
        vocabs = word_list[0]
    tsne = TSNE(n_components=2)
    X_tsne = tsne.fit_transform(X[:, :])
    df = pd.DataFrame(X_tsne, index=vocabs, columns=['x', 'y'])
    fig = plt.figure(figsize=(20, 20))
    plt.scatter(df['x'], df['y'])
    for word, pos in df.iterrows():
        plt.annotate(word, pos, size=2)
    plt.plot(markersize=1)
    plt.savefig('word2vec_sim.pdf')


plot()

# %%
# Classification


def find_group(n):
    with open('../word2vec/y_km_%d.json' % n, encoding="utf-8") as json_file:
        y_km_obj = json.load(json_file)
    with open('../ngram/unigram_keywords.json', encoding="utf-8") as json_file:
        unigram_keywords_list = list(json.load(json_file).keys())

    result = {i: [word for word in unigram_keywords_list if word in y_km_obj[f"{i}"]]
              for i in range(n)}
    print(result)
    with open('../word2vec/word_group_%d.json' % n, 'w') as json_file:
        json.dump(result, json_file, indent=4)


find_group(50)

# %%
# Re-grouping


def regroup():
    with open('../word2vec/question_answer_set.json', encoding="utf-8") as json_file:
        question_answer_set = json.load(json_file)
    with open('../ngram/unigram.json', encoding="utf-8") as json_file:
        unigrams = json.load(json_file)
    questions = list(question_answer_set.keys())
    re_group = {}
    for question in questions:
        re_group[question] = []
        for choice in question_answer_set[question]:
            choice_obj = {choice: []}
            for unigram in unigrams:
                for keyword in unigram["1gram"]:
                    if choice in keyword:
                        choice_obj[choice].append(unigram["Injury"])
            re_group[question].append(choice_obj)
    with open('../word2vec/re_group.json', 'w') as json_file:
        json.dump(re_group, json_file, indent=4)


regroup()
# %%
# Re-goruping 2
import json

def regroup2():
    with open('../word2vec/question_answer_set.json', encoding="utf-8") as json_file:
        question_answer_set = json.load(json_file)
    with open('../ngram/unigram.json', encoding="utf-8") as json_file:
        unigrams = json.load(json_file)
    questions = list(question_answer_set.keys())
    re_group2 = []
    for unigram in unigrams:
        item = {}
        item['Injury'] = unigram['Injury']
        item['Characteristics'] = {}
        for question in questions:
            item['Characteristics'][question] = []
            for key_pair in unigram['1gram']:
                keyword = list(key_pair.keys())[0]
                for word in question_answer_set[question]:
                    if keyword == word:
                        item['Characteristics'][question].append(keyword)
        re_group2.append(item)
    with open('../word2vec/re_group2.json', 'w') as json_file:
        json.dump(re_group2, json_file, indent=4)

regroup2()

# %%
# Re-grouping 3

def regroup3():
    with open('../word2vec/re_group2.json', encoding="utf-8") as json_file:
        injury_group = json.load(json_file)
    re_group3 = []
    for injury in injury_group:
        for position in injury['Characteristics']['position']:

            if len(injury['Characteristics']['trigger']) == 0:
                injury['Characteristics']['trigger'] = ['null']
            for trigger in injury['Characteristics']['trigger']:

                if len(injury['Characteristics']['feeling']) == 0:
                    injury['Characteristics']['feeling'] = ['null']
                for feeling in injury['Characteristics']['feeling']:

                    if len(injury['Characteristics']['personal info']) == 0:
                        injury['Characteristics']['personal info'] = ['null']
                    for personal_info in injury['Characteristics']['personal info']:
                        
                        if len(injury['Characteristics']['sports']) == 0:
                            injury['Characteristics']['sports'] = ['null']
                        for sport in injury['Characteristics']['sports']:
                                item = {
                                    'Injury': injury['Injury'],
                                    'Position': position,
                                    'Trigger': trigger,
                                    'Feeling': feeling,
                                    'Personal info': personal_info,
                                    'Sports': sport
                                }
                        re_group3.append(item)
    # print(re_group3)
    with open('../word2vec/re_group3.json', 'w') as json_file:
        json.dump(re_group3, json_file, indent=4)

regroup3()
# %%
