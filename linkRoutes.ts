import express from 'express';
import { linkController } from './main';


export const linkRoutes = express.Router();
linkRoutes.post('/getLink',  linkController.getLink);
