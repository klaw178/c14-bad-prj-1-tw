import express from 'express';
import { MedlogService } from '../services/MedlogService';
import { ErrorMessage, Message } from '../models/models'

export class MedlogController {
    constructor(private medlogService: MedlogService) {

    }

    saveMedLog = async (req: express.Request, res: express.Response) => {
        try {
            const userId = req.session['user'].id;
            const injury = req.session['injury'];
            const diagnosis = req.session['diagnosis'];
            await this.medlogService.saveMedLog(userId, injury, diagnosis);
            let returnMessage: Message = {
                success: true,
                message: "Saved",
            }
            res.status(200).json(returnMessage);
        } catch (e) {
            console.log(e);
            let errorMessage = new ErrorMessage();
            res.status(500).json(errorMessage);
        }

    }

    getMedLog = async (req: express.Request, res: express.Response) => {
        try {
            const userId = req.session['user'].id;
            const medlogs = await this.medlogService.getMedLog(userId);
            if (medlogs.length != 0) {
                res.status(200).json(medlogs)
            } else {
                let returnMessage: Message = {
                    success: false,
                    message: "You have currently no medical records.",
                }
                res.status(400).json(returnMessage)
            }
        } catch (e) {
            console.log(e);
            let errorMessage = new ErrorMessage();
            res.status(500).json(errorMessage);
        }
    }
}