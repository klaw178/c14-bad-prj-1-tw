import express from 'express';
import { ErrorMessage, Message } from '../models/models';
import { EditService } from '../services/EditService';

export class EditController {
    constructor(private editService: EditService) {

    }

    getLink = async (req: express.Request, res: express.Response) => {
        try {
            await this.editService.editInfo(req.body.first_name, req.body.last_name, req.body.password, req.body.qualification, req.body.occupation, req.body.phone_number, req.session['user'].id);
            let returnMessage:Message = {
                success: true,
                message: "Edited",
            }
            res.status(200).json(returnMessage);
        } catch (e) {
            console.log(e);
            let errorMessage = new ErrorMessage()
            res.status(500).json(errorMessage)
        }
        
    }
}