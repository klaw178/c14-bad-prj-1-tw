import express from 'express';
import { hashPassword } from '../hash';
import { ErrorMessage, Message } from '../models/models';
import { SignupService } from '../services/signUpService';

class SignupMessage implements Message {
    success: boolean;
    message: string;
    is_contributor: boolean;
    constructor(success: boolean, message: string, is_contributor: boolean) {
        this.success = success;
        this.message = message;
        this.is_contributor = is_contributor;
    }
}

export class SignupController {
    constructor(private signupService: SignupService) {

    }

    memberRegistration = async (req: express.Request, res: express.Response) => {
        try {
            const users = await this.signupService.checkUser(req.body.email);
            const userFound = users[0];
            if (userFound === undefined) {
                delete req.body.retypePassword
                let hashedPassword = await hashPassword(req.body.password);
                // req.body.image = req.file.filename;
                await this.signupService.insertUser(req.body, hashedPassword);
                const newUser = (await this.signupService.checkUser(req.body.email))[0];
                req.session['user'] = newUser;
                const returnMessage = new SignupMessage(true, "Signed Up", false)
                res.status(200).json(returnMessage)
            }
            else {  
                const returnMessage = new SignupMessage(false, "Already existed", false)
                res.status(400).json(returnMessage)
            }
        } catch (e) {
            console.log(e);
            let errorMessage = new ErrorMessage();
            res.status(500).json(errorMessage);
        }
    }

    contributorRegistration = async (req: express.Request, res: express.Response) => {
        try {
            const users = await this.signupService.checkUser(req.body.email);
            const userFound = users[0];
            if (userFound === undefined) {
                delete req.body.retypePassword
                let hashedPassword = await hashPassword(req.body.password);
                // req.body.image = req.file.filename;
                await this.signupService.insertUser(req.body, hashedPassword);
                const newUser = (await this.signupService.checkUser(req.body.email))[0];
                req.session['user'] = newUser;
                const returnMessage = new SignupMessage(true, "Signed Up", true)
                res.status(200).json(returnMessage)
            }
            else {
                const returnMessage = new SignupMessage(false, "Already existed", false)
                res.status(400).json(returnMessage)
            }
        } catch (e) {
            console.log(e);
            let errorMessage = new ErrorMessage();
            res.status(500).json(errorMessage);
        }
    }
}