import express from 'express';
import { checkPassword } from '../hash';
import { Message } from '../models/models';
import { LoginService } from '../services/LoginService';
import { User } from '../models/models';


class LoginMessage implements Message {
    success: boolean;
    message: string;
    is_contributor: boolean;
    constructor(success: boolean, message: string, is_contributor: boolean) {
        this.success = success;
        this.message = message;
        this.is_contributor = is_contributor;
    }
}

class LoginStatusMessage implements Message {
    success: boolean;
    message: string;
    loginStatus: boolean;

    constructor(success: boolean, message: string, loginStatus: boolean) {
        this.success = success;
        this.message = message;
        this.loginStatus = loginStatus;
    }
}

export class LoginController {
    constructor(private loginService: LoginService) {
    }
    
    // async memberLogin(req: express.Request, res: express.Response) { //refer to loginRoutes.ts
    memberLogin = async (req: express.Request, res: express.Response) => {
        console.log(this)
        const user: User[] = await this.loginService.memberLogin(req.body.email);
        const userFound = user[0]
        console.log(userFound)
        if (userFound && await checkPassword(req.body.password, userFound.password) && userFound.is_active) {
            req.session['user'] = userFound;
            let returnMessage: LoginMessage;
            if (!userFound.is_contributor) { returnMessage = new LoginMessage(true, "This is a member account.", false) }
            else { returnMessage = new LoginMessage(false, "Sorry! This is contibutor account.", true) }
            res.status(200).json(returnMessage);
        } else {
            let returnMessage: Message = {
                success: false,
                message: "Incorrect username/password"
            };
            res.status(401).json(returnMessage);
        }
    }


    contributorLogin = async (req: express.Request, res: express.Response) => {
        const user: User[] = await this.loginService.contributorLogin(req.body.email);
        const userFound = user[0]
        if (userFound && await checkPassword(req.body.password, userFound.password) && userFound.is_active) {
            req.session['user'] = userFound;
            let returnMessage: LoginMessage;
            if (userFound.is_contributor) { returnMessage = new LoginMessage(true, "This is a contributor account.", true) }
            else { returnMessage = new LoginMessage(false, "Sorry! This is member account.", false) }
            res.status(200).json(returnMessage);
        } else {
            let returnMessage: Message = {
                success: false,
                message: "Incorrect username/password"
            };
            res.status(401).json(returnMessage);
        }
    }

    getLoginStatus = async (req: express.Request, res: express.Response) => {
        if (req.session['user']) {
            let returnMessage = new LoginStatusMessage(true, "logged in", true);
            res.status(200).json(returnMessage)
        } else {
            let returnMessage = new LoginStatusMessage(false, "haven't logged in", false);
            res.status(401).json(returnMessage);
        }
    }

    logout = async (req: express.Request, res: express.Response) => {
        delete req.session['user'];
        let returnMessage: Message = {
            success: true,
            message: "log out",
        }
        res.json(returnMessage)
    }
}




