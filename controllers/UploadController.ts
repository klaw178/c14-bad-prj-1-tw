import express from 'express';
import { ErrorMessage, Message } from '../models/models';
import { UploadService } from '../services/UploadService';

class UploadMessage implements Message {
    success: boolean;
    message: string;
    upload: string;
    constructor(success: boolean, message: string, upload: string) {
        this.success = success;
        this.message = message;
        this.upload = upload;
    }
}

export class UploadController {
    constructor(private uploadService: UploadService) {

    }

    uploadProfilePic = async (req: express.Request, res: express.Response) => {
        try {
            await this.uploadService.uploadProfilePic(req.file.filename, req.session['user'].id);
            let returnMessage = new UploadMessage(true, 'Profile picture updated', 'jpeg');
            res.status(200).json(returnMessage);
        } catch (e) {
            console.log(e);
            let errorMessage = new ErrorMessage();
            res.status(500).json(errorMessage);
        }
    }

    uploadVideo = async (req: express.Request, res: express.Response) => {
        try {
            await this.uploadService.uploadVideo(req.file.filename);
            let returnMessage = new UploadMessage(true, 'Material updated', 'mp4');
            res.status(200).json(returnMessage);
        } catch (e) {
            console.log(e);
            let errorMessage = new ErrorMessage();
            res.status(500).json(errorMessage);
        }
    }
}