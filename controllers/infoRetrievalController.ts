import express from 'express';

export class InfoRetrievalController{
    constructor(){

    }

    infoRetrieval = async(req:express.Request, res:express.Response)=>{
        const userFound = req.session['user'];
        if (userFound) {
            res.json(
                userFound
            );
        } else {
            res.status(401).json({ message: "Unauthorized" });
        }
    };
}


