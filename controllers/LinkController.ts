import express from 'express';
import { ErrorMessage } from '../models/models';
import { LinkService } from '../services/LinkService';

export class LinkController {
    constructor(private linkService: LinkService) {

    }

    getLink = async (req: express.Request, res: express.Response) => {
        try {
            const link = this.linkService.getLink(req.body.injury_name);
            res.status(200).json(link);
        } catch (e) {
            console.log(e);
            let errorMessage = new ErrorMessage()
            res.status(500).json(errorMessage)
        }

    }
}