import express from 'express';
import { uploadController, upload, isLoggedIn } from './main';

export const uploadRoutes = express.Router();
uploadRoutes.post('/profile-picture', isLoggedIn, upload.single('image'), uploadController.uploadProfilePic);
uploadRoutes.post('/upload', upload.single('image'), uploadController.uploadVideo);