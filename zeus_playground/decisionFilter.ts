import dataset from "./re_group.json";

function getInjuries() {
    let allInjuries: string[] = [];
    for (let position of dataset.position) {
        let key = Object.keys(position)[0];
        let injuries = position[key];
        for (let injury of injuries) {
            allInjuries.push(injury);
        }
    }
    allInjuries.sort();
    var unique = allInjuries.filter((v, i, a) => a.indexOf(v) === i);
    return unique;
}
function filterAnswers(questions: string[], q: number, allInjuries: string[]) {
    let answers: string[] = [];
    let items = dataset[questions[q]];
    for (let item of items) {
        let ansewr = Object.keys(item)[0];
        let injuries = item[ansewr];
        injuries.sort();
        let lhs = 0; let rhs = 0;
        while (lhs < allInjuries.length && rhs < injuries.length) {
            // console.log(ansewr, lhs, allInjuries[lhs], rhs, injuries[rhs])
            if (allInjuries[lhs] === injuries[rhs]) {
                answers.push(ansewr);
                break;
            }
            if (allInjuries[lhs] > injuries[rhs] === false)
                lhs++;
            else if (allInjuries[lhs] < injuries[rhs] === false)
                rhs++;
        }
    }
    answers.sort();
    var unique = answers.filter((v, i, a) => a.indexOf(v) === i);
    return unique;
}
function filterInjuries(allInjuries: string[], question: string, answers: string[]) {
    let filteredInjuries: string[] = [];
    for (let answer of answers) {
        for (let item of dataset[question]) {
            if (item[answer]) {
                let injuries = item[answer];
                injuries.sort();
                let lhs = 0; let rhs = 0;
                // console.log(allInjuries, injuries)
                while (lhs < allInjuries.length && rhs < injuries.length) {
                    // console.log(lhs, allInjuries[lhs], rhs, injuries[rhs])
                    if (allInjuries[lhs] === injuries[rhs]) {
                        filteredInjuries.push(injuries[rhs]);
                        lhs++;
                        rhs++;
                    }
                    if (allInjuries[lhs] > injuries[rhs] === false)
                        lhs++;
                    else if (allInjuries[lhs] < injuries[rhs] === false)
                        rhs++;
                }
            }
        }
    }
    return filteredInjuries;
}

let allInjuries = getInjuries();
let questions = Object.keys(dataset);
let q = 0;

console.log(questions[q])
let answers = filterAnswers(questions, q, allInjuries);
console.log(answers)

let choices = ["knee"];
allInjuries = filterInjuries(allInjuries, questions[q], choices);
q++;
console.log(questions[q])
answers = filterAnswers(questions, q, allInjuries);
console.log(answers)

choices = ["jumping", "running"];
allInjuries = filterInjuries(allInjuries, questions[q], choices);
q++;
console.log(questions[q])
answers = filterAnswers(questions, q, allInjuries);
console.log(answers)

choices = ["swelling", "pain"];
allInjuries = filterInjuries(allInjuries, questions[q], choices);
q++;
console.log(questions[q])
answers = filterAnswers(questions, q, allInjuries);
console.log(answers)

choices = ["male"];
allInjuries = filterInjuries(allInjuries, questions[q], choices);
q++;
console.log(questions[q])
answers = filterAnswers(questions, q, allInjuries);
console.log(answers)

console.log(allInjuries)