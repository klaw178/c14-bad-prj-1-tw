// import express from 'express';

// export const decisionTreeRoutes = express.Router();


class DecisionNode {
    public question: string | null;
    public answers: Answers;
    public possibility: Map<string, string[]>;

    constructor(object: Object) {
        for (let key in object) {
            this[key] = object[key]
        }
    }

    public addInjury(question: string, answer: string, injury: Injury) {
        if (this.question) {
            if (this.question == question)
                this.addNewNode(answer, injury);
            else {
                let injury_answers = this.possibility.get(injury.injury);
                if (injury_answers) {
                    for (let injury_answer of injury_answers) {
                        let node = this.answers[injury_answer];
                        if (node == null) {
                            node = new DecisionNode({ question: null, answers: {}, possibility: new Map() });
                            node.addInjury(question, answer, injury);
                            this.answers[injury_answer] = node;
                        } else {
                            if (node instanceof DecisionNode) {
                                if (node.question == question) {
                                    node.addInjury(question, answer, injury);
                                } else {
                                    node.recursiveAdd(node, question, answer, injury);
                                }
                            }
                        }
                    }
                } else {

                }
            }
        } else {
            this.question = question;
            this.addNewNode(answer, injury);
        }
    }
    private recursiveAdd(node: DecisionNode, question: string, answer: string, injury: Injury) {
        let parent = node;
        let node_injury_answers = parent.possibility.get(injury.injury);
        if (node_injury_answers) {
            for (let node_injury_answer of node_injury_answers) {
                node = parent.answers[node_injury_answer] as DecisionNode;
                if (node == null) {
                    node = new DecisionNode({ question: null, answers: {}, possibility: new Map() });
                    node.addInjury(question, answer, injury);
                    parent.answers[node_injury_answer] = node;
                } else {
                    if (node instanceof DecisionNode) {
                        if (node.question == question)
                            node.addInjury(question, answer, injury);
                        else
                            node.recursiveAdd(node, question, answer, injury);
                    }
                }
            }
        }
    }
    private addNewNode(answer: string, injury: Injury) {
        if (!this.answers[answer])
            this.answers[answer] = null;
        if (!this.possibility.has(injury.injury))
            this.possibility.set(injury.injury, []);
        this.possibility.get(injury.injury)!.push(answer);
    }
}

class Answers {
    [choice: string]: DecisionNode | Injury | Empty | null;
}

class Empty {
    public end: boolean;

    constructor(object: Object) {
        for (let key in object) {
            this[key] = object[key]
        }
        if (!this.end) throw ("Invalid structure");
    }
}

class Injury {
    public injury: string;
    public end: boolean;

    constructor(object: Object) {
        for (let key in object) {
            this[key] = object[key]
        }
    }
}

function ParseRoot(dataset: Object): DecisionNode {
    let root = new DecisionNode({ question: null, answers: {}, possibility: new Map() });
    for (let question in dataset) {
        let items = dataset[question];
        for (let item of items) {
            for (let answer in item) {
                let possibility = item[answer];
                for (let injury of possibility) {
                    injury = new Injury({ injury: injury, end: false })
                    root.addInjury(question, answer, injury);
                }
            }
        }
    }
    return root;
}
import dataset from "./re_group.json";
// export function main(answers: string[]) {
//     let root = ParseRoot(dataset)
//     let node: DecisionNode | Injury | Answers | Empty = root as DecisionNode;
//     let reply;
//     let index = 0;
//     do {
//         if (node instanceof DecisionNode) {
//             node = QnA(node, answers[index++]);
//             if (node instanceof DecisionNode) {
//                 reply = node
//             }
//         }
//         if (node instanceof Injury) {
//             reply = node.injury;
//             if (node.end)
//                 break;
//         }
//         if (node instanceof Empty) {
//             reply = 'Sorry we cannot diagnosis';
//             if (node.end)
//                 break;
//         }
//     } while (node != null && index < answers.length);
//     return reply;
// }

// export function QnA(node: DecisionNode, answer: string): DecisionNode | Injury | Answers | Empty {
//     // console.log("Question: ", node.question)
//     // let i = 1
//     // for (let choice in node.answers) {
//     //     console.log(i++, ": ", choice)
//     // }
//     // console.log("Answer: ", answer)
//     return node.answers[answer]

// }
// class diagnosisProcess {
//     public question: string;

//     constructor(object: Object) {
//         for (let key in object) {
//             this[key] = object[key]
//         }
//     }
// }

// export function getDiagnosis(answers: string[]) {
//     let root = ParseRoot(dataset)
//     let node: DecisionNode | Injury | Answers | Empty = root as DecisionNode;
//     const diagnosis: diagnosisProcess = new diagnosisProcess({
//     })
//     let index = 0;
//     do {
//         if (node instanceof DecisionNode) {
//             diagnosis[node.question] = answers[index];
//             node = QnA(node, answers[index++]);
//         }
//         if (node instanceof Injury) {
//             if (node.end)
//                 break;
//         }
//         if (node instanceof Empty) {
//             if (node.end)
//                 break;
//         }
//     } while (node != null && index < answers.length);
//     return diagnosis;
// }

// decisionTreeRoutes.post('/decision', async function (req: Request, res: Response) {
//     const reply = main(req.body.userInput);
//     const diagnosis = getDiagnosis(req.body.userInput);
//     req.session['injury'] = reply;
//     req.session['diagnosis'] = diagnosis;
//     console.log(reply);
//     console.log(diagnosis)
//     res.status(200).json(reply)
// })

let root = ParseRoot(dataset)
console.log(root.question)
console.log(Object.keys(root.answers))
let node = root.answers["achilles"] as DecisionNode;
console.log(node.question)
console.log(Object.keys(node.answers))
node = node.answers["dorsiflexion"] as DecisionNode;
console.log(node.question)
console.log(Object.keys(node.answers))
node = node.answers["swelling"] as DecisionNode;
console.log(node.question)
console.log(Object.keys(node.answers))
console.log(node)
node = node.answers["child"] as DecisionNode;
console.log(node)
// console.log(node.question)
// console.log(Object.keys(node.answers))