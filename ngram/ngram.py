#%%
# Import all the necessary modules
import sys
import os
import json
import nltk 
import re
import unicodedata
import numpy as py
import pandas as pd
# nltk.download('punkt') #only need to download once
# nltk.download('stopwords') #only need to download once
# nltk.download('wordnet') #only need to download once
from nltk.corpus import stopwords
from collections import Counter
stop_words = set(stopwords.words('English'))

#%% 
# Text cleaning function
def basic_clean(text):
  stopwords = nltk.corpus.stopwords.words('english')
  text = (unicodedata.normalize('NFKD', text)
    .encode('ascii', 'ignore')
    .decode('utf-8', 'ignore')
    .lower())
  words = re.sub(r'[^\w\s]|[\d]', '', text).split()
  return [word for word in words if word not in stopwords]

#%%
# Generating ngram function

def create_clean_ngram(words, n):
  count_list = ((pd.Series(nltk.ngrams(words, n)).value_counts()[:100]).to_list())
  tup_list = list((pd.Series(nltk.ngrams(words, n)).value_counts()[:100]).to_dict())
  key_list = []
  for tup in tup_list:
    key_phrase = ""
    for key in tup:
      if tup.index(key) == 0:
        key_phrase += key
      else:
        key_phrase += "_" + key
    key_list.append(key_phrase)
  return [{key_list[i]:count_list[i]} for i in range(len(key_list))]

#%% 
# Generating JSON function

def gen_ngram_json(src, dest, n):
  with open(src, encoding="utf-8") as json_file:
    articles = json.load(json_file)
    ngrams = []
    for article in articles:
        text = ''
        ngram = {}
        for key in article:
            if key == 'title':
              ngram['Injury'] = article[key]
            text += article[key] 
            text += ' '
        words = basic_clean(text)
        ngram_name = f"{n}gram"
        ngram[ngram_name] = create_clean_ngram(words, n)
        ngrams.append(ngram)
    with open(dest, 'w') as json_file:
      json.dump(ngrams, json_file, indent=4)

#%% 
# Finding distinct keywords function

def check_duplicate(src, dest, n):
  with open(src, encoding="utf-8") as json_file:
    ngrams = json.load(json_file)
    keywords_list = []
    for ngram in ngrams:
      ngram_name = f"{n}gram"
      for item in ngram[ngram_name]:
        key = list(item.keys())[0]
        keywords_list.append(key)
    distinct_keywords_dict = Counter(keywords_list)
    with open(dest, 'w') as json_file:
      json.dump(distinct_keywords_dict, json_file, indent=4)

#%%
# Execute panel

gen_ngram_json('../scrapping/filteredContent.json', '../ngram/unigram.json', 1)
gen_ngram_json('../scrapping/filteredContent.json', '../ngram/bigram.json', 2)
check_duplicate('../ngram/unigram.json', '../ngram/unigram_keywords.json', 1)
check_duplicate('../ngram/bigram.json', '../ngram/bigram_keywords.json', 2)

# %%
