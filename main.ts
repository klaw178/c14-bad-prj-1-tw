import express from 'express';
import { Request, Response } from 'express';
import expressSession from 'express-session';
import multer from 'multer';
import path from 'path';
import dotenv from 'dotenv';
import Knex from 'knex';

const knexConfigs = require('./knexfile');
export const knex = Knex(knexConfigs[process.env.NODE_ENV || "development"]);
const app = express();
dotenv.config();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(expressSession({
    secret: 'Welcome to Freezio',
    resave: true,
    saveUninitialized: true
}));

const storage = multer.diskStorage({
    destination: function (req: Request, file, cb) {
        cb(null, path.resolve('uploads'));
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})
export const upload = multer({ storage })

export const isLoggedIn = function (req: Request, res: Response, next: express.NextFunction) {
    if (req.session['user']) {
        next();
    } else {
        console.log('is redirected by isLoggedIn middleware');
        res.redirect('/');
    }
}

app.get('/', (req, res) => {
    res.sendFile(path.resolve('./public/mainpage.html'))
})


import { LoginService } from './services/LoginService';
import { LoginController } from './controllers/LoginController';
export const loginService = new LoginService(knex);
export const loginController = new LoginController(loginService);

import { SignupService } from './services/SignupService';
import { SignupController } from './controllers/SignupController';
export const signupService = new SignupService(knex);
export const signupController = new SignupController(signupService);

import { LinkService } from './services/LinkService';
import { LinkController } from './controllers/LinkController';
export const linkService = new LinkService(knex);
export const linkController = new LinkController(linkService);

import { UploadService } from './services/UploadService';
import { UploadController } from './controllers/UploadController';
export const uploadService = new UploadService(knex);
export const uploadController = new UploadController(uploadService);

import { MedlogService } from './services/MedlogService';
import { MedlogController } from './controllers/MedlogController';
export const medlogService = new MedlogService(knex);
export const medlogController = new MedlogController(medlogService);

import { InfoRetrievalController } from './controllers/InfoRetrievalController'
export const infoRetrievalController = new InfoRetrievalController();

import { decisionTreeRoutes } from './decisionTree';
import { linkRoutes } from './linkRoutes';
import { signupRoutes } from './signupRoutes';
import { loginRoutes } from './loginRoutes';
import { uploadRoutes } from './uploadRoutes';
import { medlogRoutes } from './medlogRoutes';
import { infoRetrievalRoutes } from './infoRetrievalRoutes';

app.use(decisionTreeRoutes);
app.use(linkRoutes);
app.use(signupRoutes);
app.use(loginRoutes);
app.use(uploadRoutes);
app.use(medlogRoutes);
app.use(infoRetrievalRoutes);

app.use(express.static('public'));
app.use(isLoggedIn, express.static('uploads'));
app.use(isLoggedIn, express.static('private'));

const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});