import { infoRetrievalController } from './main';
import express from 'express';
import { isLoggedIn } from './main';

export const infoRetrievalRoutes = express.Router();

infoRetrievalRoutes.get('/infoRetrieval', isLoggedIn, infoRetrievalController.infoRetrieval)