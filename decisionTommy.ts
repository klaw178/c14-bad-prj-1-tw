import Knex from 'knex';
import dotenv from 'dotenv';

dotenv.config();
const knexConfigs = require('./knexfile');
const knex = Knex(knexConfigs[process.env.NODE_ENV || "development"]);

export async function positionFindTrigger(position:string){
    const objects = await knex.distinct('Trigger').where('Position', position).from('injury')
    let result = {}
    result['position'] = position
    result['trigger'] = []
    for (let object of objects){
        if (object['Trigger'] !== 'null'){
            result['trigger'].push(object['Trigger'])
        }
    }
    return result
}

export async function triggerFindFeeling(answer:{}){
    if (answer['trigger'].length === 0){
        const injury = await knex.distinct('Injury').where('Position', answer['position']).from('injury')
        return injury
    }
    const objects = await knex.distinct('Feeling').whereIn('Trigger', answer['trigger']).where('Position', answer['position']).from('injury')
    answer['feeling'] = []
    for (let object of objects){
        if (object['Feeling'] !== 'null'){
            answer['feeling'].push(object['Feeling'])
        }
    }
    if (answer['feeling'].length === 0){
        const injury = await knex.distinct('Injury').whereIn('Trigger', answer['trigger']).where('Position', answer['position']).from('injury')
        answer['injury'] = injury
        return answer
    }
    return answer
}

export async function feelingFindInfo(answer:{}){
    if (answer['feeling'].length === 0){
        const injury = await knex.distinct('Injury').whereIn('Trigger', answer['trigger']).where('Position', answer['position']).from('injury')
        return injury
    }
    const objects = await knex.distinct('Personal_info').whereIn('Feeling', answer['feeling']).whereIn('Trigger', answer['trigger']).where('Position', answer['position']).from('injury')
    answer['personal info'] = []
    for (let object of objects){
        if (object['Personal_info'] !== 'null'){
            answer['personal info'].push(object['Personal_info'])
        }
    }
    if (answer['personal info'].length === 0){
        const injury = await knex.distinct('Injury').whereIn('Feeling', answer['feeling']).whereIn('Trigger', answer['trigger']).where('Position', answer['position']).from('injury')
        answer['injury'] = injury
        return answer
    }
    return answer
}

export async function infoFindSports(answer:{}){
    if (answer['personal info'].length === 0){
        const injury = await knex.distinct('Injury').whereIn('Feeling', answer['feeling']).whereIn('Trigger', answer['trigger']).where('Position', answer['position']).from('injury')
        return injury
    }
    const objects = await knex.distinct('Sports').whereIn('Personal_info', answer['personal info']).whereIn('Feeling', answer['feeling']).whereIn('Trigger', answer['trigger']).where('Position', answer['position']).from('injury')
    answer['sports'] = []
    for (let object of objects){
        if (object['Sports'] !== 'null'){
            answer['sports'].push(object['Sports'])
        }
    }
    if (answer['sports'].length === 0){
        const injury = await knex.distinct('Injury').whereIn('Personal_info', answer['personal info']).whereIn('Feeling', answer['feeling']).whereIn('Trigger', answer['trigger']).where('Position', answer['position']).from('injury')
        answer['injury'] = injury
        return answer
    }
    return answer
}

export async function sportsFindInjury(answer:{}){
    if (answer['sports'].length === 0){
        const injury = await knex.distinct('Injury').whereIn('Personal_info', answer['personal info']).whereIn('Feeling', answer['feeling']).whereIn('Trigger', answer['trigger']).where('Position', answer['position']).from('injury')
        return injury
    }
    const objects = await knex.distinct('Injury').whereIn('Sports', answer['sports']).whereIn('Personal_info', answer['personal info']).whereIn('Feeling', answer['feeling']).whereIn('Trigger', answer['trigger']).where('Position', answer['position']).from('injury')
    answer['injury'] = []
    for (let object of objects){
        if (object['Injury'] !== 'null'){
            answer['injury'].push(object['Injury'])
        }
    }
    return answer
}

positionFindTrigger('achilles').then(console.log)

triggerFindFeeling({
    position: 'achilles',
    trigger: [
      'dorsiflexion',
      'flexion',
      'jumping',
      'pushing'
    ]
  }).then(console.log)

feelingFindInfo({
    position: 'achilles',
    trigger: [ 'dorsiflexion', 'flexion', 'jumping', 'pushing' ],
    feeling: [ 'pain', 'tenderness']
}).then(console.log)

infoFindSports({
    position: 'achilles',
    trigger: [ 'dorsiflexion', 'flexion', 'jumping', 'pushing' ],
    feeling: [ 'pain', 'tenderness' ],
    'personal info': []
}).then(console.log)