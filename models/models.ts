export interface User {
    id: number;
    email: string;
    password: string;
    last_name: string;
    first_name: string;
    phone_number: string;
    is_active: boolean;
    is_contributor: boolean;
}

export interface Message {
    success: boolean;
    message: string;
}

export class ErrorMessage {
    success: boolean;
    message: string;
    constructor() {
        this.success = false;
        this.message = "Internal Error"
    }
}

