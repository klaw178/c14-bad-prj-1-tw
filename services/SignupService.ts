import { Knex } from 'knex';
import { User } from '../models/models';

export class SignupService {
    constructor(private knex: Knex) {

    }

    async checkUser(email: string) {
        const users: User[] = await this.knex.select('*').from('users').where('email', email);
        return users;
    }

    async insertUser(user:User, hashedPassword:string) {
        user.password = hashedPassword
        const id = await this.knex
            .insert([user])
            .into('users')
            .returning('id');
        return id;
    }

}