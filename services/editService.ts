import { Knex } from 'knex';

export class EditService {
    constructor(private knex: Knex) {

    }
    async editInfo(first_name: string, last_name: string, password: string, qualification: string, occupation: string, phone_number: number, id: number) {
        await this.knex('users')
            .where('id', id)
            .update({
                first_name: first_name,
                last_name: last_name,
                password: password,
                qualification: qualification,
                occupation: occupation,
                phone_number: phone_number
            })
    }

}