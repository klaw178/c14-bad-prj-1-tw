import { Knex } from 'knex';


export class UploadService {
    constructor(private knex: Knex) {

    }

    async uploadProfilePic(filename: string, id: number) {
        await this.knex('users').where('id', id).update({
            image: filename
        })
    }

    async uploadVideo(filename: string) {
        console.log(filename)
    }
}

