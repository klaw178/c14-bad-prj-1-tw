import { Knex } from 'knex';

export class LinkService{
    constructor(private knex: Knex){
        
    }
    async getLink(injury:string){
        try {
            const link = (await this.knex('injuries').select('*').where('injury_name' , injury))[0]['link'];
            return link
        } catch (e) {
            console.log('Error: ', e)
        }
        
    }

}