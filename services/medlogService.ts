import { Knex } from 'knex';



export class MedlogService{
    constructor(private knex: Knex){
        
    }
    async saveMedLog(userId:number, injury:string, diagnosis:string){
        const injuryId = (await this.knex('injuries').select('id').where('injury_name' , injury))[0]
        await this.knex('medlog').insert({
            user_id: userId,
            injury_id: injuryId,
            diagnosis: diagnosis
        })
        return 
    }

    async getMedLog(userId:number){
        const result = (await this.knex('medlog').select('*').innerJoin('users', 'users.id', 'user_id').where('user_id', userId))[0]
        return result
    }
}
