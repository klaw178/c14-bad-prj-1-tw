import { Knex } from 'knex';

export class LoginService{
    constructor(private knex: Knex){
        
    }
    async memberLogin(email:string){
        const result = await this.knex('users').select('*').where('email' , email)
        return result
    }

    async contributorLogin(email:string){
        const result = await this.knex('users').select('*').where('email' , email)
        return result

    }
}