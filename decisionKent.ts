import Knex from 'knex';
import dotenv from 'dotenv'

dotenv.config()

const knexConfigs = require('./knexfile');
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode];
const knex = Knex(knexConfig);

export async function destroyKnex() {
    await knex.destroy()
}

export async function init() {
    const choices = await knex.distinct('Position').from('injury');
    console.log(choices);
    destroyKnex()
}

export async function getTrigger(answer:string) {
    const filteredInjury = await knex.distinct('Injury').from('injury').where('Position', answer);
    console.log(filteredInjury.map(injury => {Object.keys(injury)}))
    const triggers = await knex.distinct('Trigger').from('injury').whereIn('Injury', filteredInjury)
    let result = [filteredInjury, triggers]
    return result
}

export async function getFeeling(answers:string[], prevFilteredInjury:string[]) {
    const filteredInjury = await knex.distinct('Injury').from('injury').whereIn('Trigger', answers).whereIn('Injury', prevFilteredInjury);
    const feelings = await knex.distinct('Feeling').from('injury').whereIn('Injury', filteredInjury)
    let result = [filteredInjury, feelings]
    return result
}
export async function getPersonalInfo(answers:string[], prevFilteredInjury:string[]) {
    const filteredInjury = await knex.distinct('Injury').from('injury').whereIn('Feeling', answers).whereIn('Injury', prevFilteredInjury);
    const personalInfo = await knex.distinct('Personal_info').from('injury').whereIn('Injury', filteredInjury)
    let result = [filteredInjury, personalInfo]
    return result
}
export async function getSports(answers:string[], prevFilteredInjury:string[]) {
    const filteredInjury = await knex.distinct('Injury').from('injury').whereIn('Position', answers).whereIn('Injury', prevFilteredInjury);
    const sports = await knex.distinct('Sports').from('injury').whereIn('Injury', filteredInjury)
    let result = [filteredInjury, sports]
    return result
}

export async function main(answers: any[], filteredInjury: string[] = []) {
    if (answers[3]) {
        const reply = await getSports(answers[3], filteredInjury)
        return reply
    } else if (answers[2]) {
        const reply = await getPersonalInfo(answers[2], filteredInjury)
        return reply
    } else if (answers[1]) {
        const reply = await getFeeling(answers[1], filteredInjury)
        return reply
    } else if (answers[0]) {
        const reply = await getTrigger(answers[0])
        console.log(reply)
        return reply
    }

    return
}




