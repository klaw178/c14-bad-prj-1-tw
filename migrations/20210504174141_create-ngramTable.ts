import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasNgramTable = await knex.schema.hasTable("ngram");
    if(!hasNgramTable){
        await knex.schema.createTable("ngram",(table)=>{
            table.increments();
            table.string("injury_name").notNullable();
            table.string("keyword").notNullable();
            table.integer("occurence").notNullable();
            table.timestamps(false,true);
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("ngram");
}

