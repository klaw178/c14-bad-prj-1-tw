import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("medlog");
    if (!hasTable){
            await knex.schema.createTable('medlog', (table)=>{
            table.increments();
            table.integer('user_id');
            table.foreign('user_id').references('users.id')
            table.integer('injury_id');
            table.foreign('injury_id').references('injury.id')
            table.jsonb('diagnosis');
            table.timestamps(false,true);
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("medlog");
}

