import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("users");
    if (!hasTable){
            await knex.schema.createTable('users', (table)=>{
            table.increments();
            table.string("last_name").notNullable();
            table.string("first_name").notNullable();
            table.string("email").notNullable();
            table.string("password").notNullable();
            table.integer("phone_number");
            table.string("sex");
            table.date("age"); 
            table.string("occupation");
            table.string("qualification");
            table.string("experience");
            table.boolean("is_active").notNullable();
            table.boolean("is_contributor").notNullable();
            table.string("image");
            table.timestamps(false,true);
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("users");
}

