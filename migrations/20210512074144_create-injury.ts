import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("injury");
    if (!hasTable){
            await knex.schema.createTable('injury', (table)=>{
            table.increments();
            table.string("Injury").notNullable();
            table.string("Position").notNullable();
            table.string("Trigger").notNullable();
            table.string("Feeling").notNullable();
            table.string("Personal_info").notNullable();
            table.string("Sports").notNullable();
            table.timestamps(false,true);
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("injury");
}

