import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasInjuriesTable = await knex.schema.hasTable("injuries");
    if(!hasInjuriesTable){
        await knex.schema.createTable("injuries",(table)=>{
            table.increments();
            table.string("injury_name").notNullable();
            table.timestamps(false,true);
        });  
    }
    const hasDiagnosisTable = await knex.schema.hasTable('diagnosis');
    if(!hasDiagnosisTable){
        await knex.schema.createTable('diagnosis',(table)=>{
            table.increments();
            table.integer("injury_id").unsigned();
            table.foreign("injury_id").references("injuries.id");
            table.string("location_0").notNullable();
            table.string("location_1").notNullable();
            table.string("location_2").notNullable();
            table.string("trigger").notNullable();
            table.boolean("option_stiffness");
            table.boolean("option_tenderness");
            table.boolean("option_swelling");
            table.boolean("option_bruising");
            table.timestamps(false,true);
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("diagnosis");
    await knex.schema.dropTableIfExists("injuries");
}

