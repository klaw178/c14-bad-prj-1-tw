import express from 'express';
import { isLoggedIn, medlogController } from './main';


export const medlogRoutes = express.Router();
medlogRoutes.get('/saveMedLog', isLoggedIn, medlogController.saveMedLog);
medlogRoutes.get('/getMedLog', isLoggedIn, medlogController.getMedLog);