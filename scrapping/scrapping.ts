import {chromium} from 'playwright';
// import fs from 'fs';
import jsonfile from 'jsonfile';
// const fsPromises = fs.promises;

async function main(){
    const browser = await chromium.launch({
        headless: false
    });
    const page = await browser.newPage();
    // await page.goto('https://physio-pedia.com/Articles');
    // await page.goto('https://physio-pedia.com/Category:Sports_Injuries')
    await page.goto('https://physio-pedia.com/index.php?title=Category:Sports_Injuries&pagefrom=Spondylolysis+in+Young+Athletes#mw-pages')
    await page.waitForTimeout(5000);
    const paths = await page.evaluate(()=>{
        let result:any[] = []
        // let anchors = document.querySelectorAll('.hw-responsive-gallery-table a')
        let anchors = document.querySelectorAll('.mw-category a')
        anchors.forEach((anchor)=>{
            result.push(anchor.getAttribute('href'))
        })
        return result
    })

    for (let path of paths){
        await page.goto(`https://physio-pedia.com${path}`);
        let result = await page.evaluate(()=>{

            const title = (document.querySelector('#firstHeading')as HTMLElement).innerText;
            let result = new Object()
            result = {
                title : title,
            }

            const headers = document.querySelectorAll('.mw-parser-output > * > .mw-headline')
            let headerIds:string[] = []
            headers.forEach((header:HTMLElement)=>{
                const headerId = header.getAttribute('id') as string
                headerIds.push(headerId)
            })
            console.log(headerIds)
            
            for (let i = 0; i<headerIds.length-1; i++){
                if (headerIds[i].includes('Description') || headerIds[i].includes('Introduction') || headerIds[i].includes('Definition') || headerIds[i].includes('Clinical_Presentation')){
                    const currentHeaderId = document.getElementById(`${headerIds[i]}`) as HTMLElement
                    let currentNode = (currentHeaderId).parentNode as HTMLElement
                    const endNode = (document.getElementById(`${headerIds[i+1]}`) as HTMLElement).parentNode as HTMLElement
                    result[currentHeaderId.innerText] = ''
                    while (currentNode.nextSibling!=endNode){
                        const paragraph = (currentNode.nextSibling as HTMLElement).innerText
                        if (paragraph){
                            result[currentHeaderId.innerText] += paragraph.replace(/\[.*?\]/g,'') + '\n'
                        }
                        currentNode= currentNode.nextSibling as HTMLElement
                    }
                }
            }

            // for (let i = 0; i<headerIds.length-1; i++){
            //     const currentHeaderId = document.getElementById(`${headerIds[i]}`) as HTMLElement
            //     let currentNode = (currentHeaderId).parentNode as HTMLElement
            //     const endNode = (document.getElementById(`${headerIds[i+1]}`) as HTMLElement).parentNode as HTMLElement
            //     result[currentHeaderId.innerText] = ''
            //     while (currentNode.nextSibling!=endNode){
            //         const paragraph = (currentNode.nextSibling as HTMLElement).innerText
            //         if (paragraph){
            //             result[currentHeaderId.innerText] += paragraph.replace(/\[.*?\]/g,'') + '\n'
            //         }
            //         currentNode= currentNode.nextSibling as HTMLElement
            //     }
            // }

            return result
            // return JSON.stringify(result)

        })

        const results = await jsonfile.readFile('./scrapping/articles.json');
        results.push(result);
        await jsonfile.writeFile('./scrapping/articles.json', results, {spaces:4});

        // fsPromises.readFile('./articles.json', 'utf-8')
        // .then((file)=>{
        //     console.log(file)
        //     console.log(result)
        //     console.log(JSON.parse(file))
        //     const array = JSON.parse(file).push(result)
        //     console.log(array)
        //     fs.writeFileSync('./articles.json',JSON.stringify(array))
        // })
        // .catch((e)=>{console.log(e)})
    }
}
main()