import jsonfile from 'jsonfile';

async function filtering() {
    const results = await jsonfile.readFile('./scrapping/articles.json');
    const filteredResults = []
    for (let result of results) {
        if (Object.keys(result).length == 3) {
            filteredResults.push(result);
        }
    }
    await jsonfile.writeFile('./scrapping/filteredContent.json', filteredResults, { spaces: 4 });
}

filtering()
