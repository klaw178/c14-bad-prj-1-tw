import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("users").del();

    // Inserts seed entries
    await knex("users").insert([{
            last_name: "Law", 
            first_name: "Kent", 
            email: "kent@kent.com", 
            password:"$2a$10$PHS1dPCKss3QfqNGQNj5K.T7DnbaMr2ihaODBxHR9EJYH7fd07L0m",
            is_active: true,
            is_contributor: true
        },{
            last_name: "Fong", 
            first_name: "Tommy", 
            email: "tommy@tommy.com", 
            password:"$2a$10$gyrVYpJG7dVuJrlM1qQjfOpC6u3NqdW6.QFiQ6Ktne4YC31vRxPVi",
            is_active: true,
            is_contributor: true
        },{
            last_name: "123", 
            first_name: "123", 
            email: "123@123.com", 
            password:"$2a$10$kaL4TA08Eoe8ZdlpfrdMmeoO9rQF6Gi1qXjrax/0qvXLzkSSkqnmS",
            is_active: true,
            is_contributor: false
        },{
            last_name: "111", 
            first_name: "111", 
            email: "111@111.com", 
            password:"$2a$10$kaL4TA08Eoe8ZdlpfrdMmeoO9rQF6Gi1qXjrax/0qvXLzkSSkqnmS",
            is_active: true,
            is_contributor: false
        }
    ]);
};
