import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("medlog").del();

    // Inserts seed entries
    await knex("medlog").insert([{
            user_id: "25", 
            injury_id: "10",
            diagnosis: {}
        },{
            user_id: "26", 
            injury_id: "10",
            diagnosis: {}
        },{
            user_id: "27", 
            injury_id: "10",
            diagnosis: {}
        },{
            user_id: "28", 
            injury_id: "10",
            diagnosis: {}
        }
    ]);
};
