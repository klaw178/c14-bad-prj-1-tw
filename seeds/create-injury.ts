import { Knex } from "knex";
import  fs from 'fs';
const fsPromises = fs.promises;

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("injury").del();

    const injuries = await fsPromises.readFile('./word2vec/re_group3.json', 'utf-8');
    let injuries2 = JSON.parse(injuries)
    for (let injury of injuries2){
        injury["Personal_info"] = injury["Personal info"]
        delete injury["Personal info"]
    }
    // console.log(injury)
    // Inserts seed entries
    await knex("injury").insert(injuries2);
};

